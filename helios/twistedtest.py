from twisted.internet.protocol import Protocol, ClientFactory, ReconnectingClientFactory
#import socket
#from threading import Thread
import time
from Crypto.Cipher import ARC4
import json
import math
#from django.db import models, transaction

class SomeServerProtocol(basic.LineReceiver):

    def __init__(self, factory):
        self.factory = factory

    def connectionMade(self):
        self.factory.currentleader = self.transport.getPeer().host
        if self in self.factory.connections:
            #run djangosync here because this was a server that disconnected
            pass
        else:
            self.factory.connections.append(self)
        print "SERVER FACTORY CONNECTIONS:",self.factory.connections

    def lineReceived(self, line):
        arc2 = ARC4.new('95616')
        json_data = json.loads(arc2.decrypt(line))   #decrypt the message
        print json_data
        if json_data[1] is None:
            print "Invalid message sent to server"
            return
        messagetype = json_data [0]     #ACC/LRN/PREP/PROM
        roundnumber = json_data[1]      #round number of the message being sent
        if roundnumber <= self.factory.highestroundnumber:   #if the server doesn't have the highest round number, we can ignore it
            print "New server doesn't have the higher round number, ignoring the message"
            return
        else:
            self.factory.highestroundnumber = roundnumber   #we have a larger roundnumber now
        arc = ARC4.new('96516')
        if messagetype == 'ACC':            #put LRN message in message queue back
            #ACC message = ["ACC",roundnumber,classname,sequencenumber]
            if self.factory.currentleader != self.transport.getPeer().host:
                print "Somehow receiving ACC from someone who's not the leader"
                return
            classname = json_data[2]
            self.factory.potentialrequest = [roundnumber, classname]
            response = json_data
            response[0] = 'LRN'
            print response
            cipherresponse = arc.encrypt(json.dumps(response))      #encrypt the message
            self.sendLine(cipherresponse)               #send the client back a LRN message
        elif messagetype == 'PREP':  #leadership change request
			#PREP message = ["PREP",roundnumber]
            #ignore all messages sent by the old leadership
            #send back <PROM, message, newleader> if there's a previous message
            #send back <PROM, None, newleader> if there isn't
            self.clearLineBuffer()
            serverfactory.currentleader = self.transport.getPeer().host		#set the new currentleader to the new peer that sent the PREP
            if self.factory.potentialrequest[1] is not None:			#if there was a previous message that wasn't fully ACCed, need to reACC it
                response = json.dumps(['PROM',self.factory.potentialrequest[1],self.factory.potentialrequest[0]])		#potentialrequest[1] = classname, potentialrequest[0] = roundnumber
            else:
                response = json.dumps(['PROM',None,self.factory.highestroundnumber])	#otherwise just regular PROM message
            cipherresponse = arc.encrypt(response)      #encrypt the message
            self.sendLine(cipherresponse)               #send the client back a PROM message
        else:
            print "Invalid server message type"
            return

    #def connectionLost(self, reason):
        #self.factory.connections.remove(self)
        
class TestServerFactory(protocol.ServerFactory):
    #protocol = SomeServerProtocol
    
    def __init__(self):
        self.connections = []
        self.highestroundnumber = 0  #the highest round number that the server has seen
        self.potentialrequest = [0, None]  #let new leader know of executed requests from previous server
        self.currentleader = None
        """
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("gmail.com",80))
        ipaddress = s.getsockname()[0]
        for server in servers:
            if server[1] == ipaddress:
                result = server[0]
        if result is not None:
            self.currentleader = result		#set the current leader to be this server for now
        else:
            raise ValueError("This server is not in the list of servers")
        """
        
    def buildProtocol(self, addr):
        return SomeServerProtocol(self)
   
 
class SomeClientProtocol(basic.LineReceiver):
    
    def __init__(self, factory):
        self.factory = factory

    def connectionMade(self):           #upon making a connection with the server, send the message
        self.factory.connections.append(self)
        host = self.transport.getHost().host
        self.factory.thisserver = host
        for index,server in enumerate(servers):
            if server[1] == host:
                self.factory.roundnumber = index % len(servers)
        if self.factory.roundnumber == 0 or host is None:
            "Some error happened with initializing the client factory variables"
            return
        print "MESSAGE",self.factory.message

        self.sendLine("This is a test from "+host)

    def lineReceived(self,line):
        print line
        arc2 = ARC4.new('95616')
        json_data = json.loads(arc2.decrypt(line))   #decrypt the message
        print json_data
        if json_data[0] == 'LRN' and self.factory.accflag == 1:     
            if time.time() > self.factory.timeout:
                    print "LRN receive timeout, did not save"
                    reset()
                    return
            self.factory.lrns_received += 1
            print >>sys.stderr, 'received "%s"' % json_data[0]
            
            if self.factory.lrns_received >= self.factory.lrns_expected:
                #save here
                roundnumber = json_data[1]
                actualsave(classobjdict[roundnumber][0],classobjdict[roundnumber][1]) 	#save(classtype,obj)
                #actualsave(classobjdict[self.factory.roundnumber][0],classobjdict[self.factory.roundnumber][1]) #save(classtype,obj)
                timetosave = time.time() - self.factory.initialtime
                with open("timetosave.txt", "a") as text_file:
                    text_file.write("Time to save with {} servers: {}".format(len(servers),timetosave))		#write how long it took for an item to save in an output file
                reset()
                print "SAVED"
            else:
                print "Not enough LRNs yet:",self.factory.lrns_received,"LRNs received"
                timetoLRN = time.time() - self.factory.initialtime
                with open("timetosave.txt", "a") as text_file:
                    text_file.write("Time to receive {}th LRN with {} servers: {}".format(self.factory.lrns_received,len(servers),timetoLRN))		#write how long it took for an item to receive a LRN
                """
            else:
                print "Not enough LRNs, resending message"
                json_data[3] = self.factory.highestroundnumber + len(servers)     #increase round number of this message
                json_data[0] = "PREP"
                arc = ARC4.new('96516')
                cipherresponse = arc.encrypt(json.dumps(json_data))
                sendmessage(cipherresponse) #need to retry a new round of Paxos to resubmit this message
				"""
        elif json_data[0] == 'LRN' and self.factory.accflag == 0:
            print "Not looking for LRNs"
            return
        
        elif json_data[0] == 'PROM' and self.factory.prepflag == 1:
            #TODO: if you receive a PROM message with a classname (that's not None), then you need to send an ACC to everyone with the classname so they can save the message (do we need to pass the obj?)
            if time.time() > self.factory.timeout:
                    print "PROM receive timeout"
                    self.factory.prepflag = 0
                    return
            self.factory.proms_received += 1
            print >>sys.stderr, 'received "%s"' % json_data[0]
            
            if self.factory.proms_received >= self.factory.proms_expected:	#got enough PROM messages, can now send ACCs
                self.factory.proms_received = 0
                self.factory.timeout = float("inf")
                self.factory.prepflag = 0
                roundnumber = json_data[2]
                classname = classobjdict(roundnumber)[1].__class__.__name__
                print "classname:",classname
                if json_data[1] is None:       #no previous message in "buffer", so can just go ahead and ACC the current message
                    sendaccmessage(json.dumps(["ACC",roundnumber,classname,self.factory.seqno]))
                else:                               #previous message in "buffer" needs to be reACCed first
                    self.factory.seqno += 1
                    classobjdict[roundnumber+len(server)] = (classobjdict(roundnumber)[0],classobjdict(roundnumber)[1])
                    sendaccmessage(json.dumps(["ACC",roundnumber+len(server),json_data[1],self.factory.seqno]))
                    """
                    #can do protocol.sendLine for each protocol in connection list
                    for protocol in self.factory.connections:
                        arc = ARC4.new('96516')
                        cipherresponse = arc.encrypt(json.dumps('ACC', classname, self.factory.highestseqno, self.factory.highestroundnumber))    #encrypt the message
                        protocol.sendLine(cipherresponse)
                        """
            else:
                print "Not enough PROMs yet:",self.factory.proms_received,"PROMs received"
                timetoPROM = time.time() - self.factory.initialtime
                with open("timetosave.txt", "a") as text_file:
                    text_file.write("Time to receive {}th PROM with {} servers: {}".format(self.factory.proms_received, len(servers),timetoPROM))		#write how long it took for an item to receive a PROM				
        elif json_data[0] == 'PROM' and self.factory.prepflag == 0:
            print "Not looking for PROMs"
            return
        else:
            print "Invalid message type"
                
    def reset(self):
        self.factory.timeout = float('inf')
        self.factory.lrns_received = 0
        self.factory.proms_received = 0
        self.factory.accflag = 0
        self.factory.prepflag = 0
        self.factory.initialtime = 0
        serverfactory.potentialrequest = [0, None]
        
    def connectionLost(self, reason):
        self.factory.connections.remove(self)
        
class TestClientFactory(ReconnectingClientFactory):
    initialDelay = 3
    factor = 1.5
    
    def __init__(self):
        self.connections = []
        self.roundnumber = 0
        self.timeout = float('inf')
        self.lrns_received = 0
        self.proms_received = 0
        self.highestroundnumber = 0 
        self.seqno = 0
        self.accflag = 0
        self.prepflag = 0
        self.thisserver = None
        initialtime = 0
        
    def startedConnecting(self, connector):
        print 'Started to connect.'

    def buildProtocol(self, addr):
        print 'Connected.'
        print 'Resetting reconnection delay'
        self.resetDelay()
        return SomeClientProtocol(self)
        
    def clientConnectionLost(self, connector, reason):
        print 'Lost connection.  Reason:', reason
        """
        if self.thisserver != self.currentleader:
            self.roundnumber = self.roundnumber + len(servers)
            message = json.dumps('PREP', self.roundnumber)
            sendmessage(message, self.classtype, self.obj)
            #currentleader = servers[0][0]   #try to claim to be the current leader 
            #only change currentleader if receive enough PROM messages??
            """
        ReconnectingClientFactory.clientConnectionLost(self, connector, reason)
     

    def clientConnectionFailed(self, connector, reason):
        print 'Connection failed. Reason:', reason
        """
        if self.thisserver != self.currentleader:
            self.roundnumber = self.roundnumber + len(servers)
            message = json.dumps('PREP', self.roundnumber)
            sendmessage(message, self.classtype, self.obj)
            #currentleader = servers[0][0]   #try to claim to be the current leader 
            #only change currentleader if receive enough PROM messages??
            """
        ReconnectingClientFactory.clientConnectionFailed(self, connector,
                                                         reason)            
	
    #def setInitialTime(self,initialtime):
     #   self.initialtime = initialtime

def actualsave(classtype, obj):
    for currentserver in servers:
        super(classtype,obj).save(using=currentserver[0])
    #close connection?
 
def findleader(roundnumber):        #figure out leader name from round number
#need to standardize server list across servers?
    serverno = roundnumber % len(servers)
    return servers[serverno][0]

def sendprepmessage(classtype, obj):
    with open("timetosave.txt", "a") as text_file:
        text_file.write("Saving {} with {} servers".format(obj.__class__.__name__,len(servers)))	
    clientfactory.initialtime = time.time()
    clientfactory.seqno += 1
    classobjdict[clientfactory.roundnumber] = (classtype,obj)
    if serverfactory.currentleader != clientfactory.thisserver:	#if the current leader isn't the client, then send a PREP message
        clientfactory.roundnumber = clientfactory.roundnumber + len(server)
        clientfactory.prepflag = 1
        arc = ARC4.new('96516')
        cipherresponse = arc.encrypt(json.dumps(["PREP",clientfactory.roundnumber]))
        clientfactory.timeout = time.time() + 10
        for connection in clientfactory.connections:
            if connection.transport.getPeer().host != clientfactory.thisserver:       #send the PREP message to every server but this one
                connection.sendLine(cipherresponse)
    else:			#the client is the current leader, and we don't need to PREP, so we just send the ACC message instead
        classname = obj.__class__.__name__
        print classname
        sendaccmessage(json.dumps(["ACC",clientfactory.roundnumber,classname,clientfactory.seqno]))
	
def sendaccmessage(message):
    arc = ARC4.new('96516')
    clientfactory.timeout = time.time() + 10
    if serverfactory.currentleader != clientfactory.thisserver:	#if the current leader isn't the client, then send a PREP message
        print "Somehow an ACC message was sent without the client factory being the leader"
        return
    else:			#the client is the current leader, so now we send an ACC message
        message1 = json.loads(message)
        if message1[0] == 'ACC':     #look for LRN response
            # Look for the response
            clientfactory.accflag = 1     #start looking for LRN messages     
        else:
            print "Invalid message type"
            return           
        cipherresponse = arc.encrypt(message)     #encrypt the message
    for connection in clientfactory.connections:
        if connection.transport.getPeer().host != clientfactory.thisserver:       #send the PREP message to every server but this one
            connection.sendLine(cipherresponse)

servers = [('server1','192.168.170.128',8000),('server2','192.168.170.130',8000),('server3','192.168.170.131',8000)]
lrns_expected = math.ceil(len(servers)/2.0)-1  #self server counts as an implicit LRN (REMEMBER TO ADD -1 WHEN INTRODUCING MORE SERVERS)
proms_expected = math.ceil(len(servers)/2.0) -1    #self server counts as an implicit PROM

serverfactory = TestServerFactory()
clientfactory = TestClientFactory()
classobjdict = {}
            

def main():
    import sys
    from twisted.python import log
    from twisted.internet import protocol, reactor
    from twisted.protocols import basic
    from twisted.internet.endpoints import TCP4ClientEndpoint, connectProtocol


    log.startLogging(sys.stdout)
    #factory = protocol.ServerFactory()
    #factory.protocol = SomeServerProtocol
    reactor.listenTCP(8000, serverfactory)
    
    #IN SAVE() function, do this, with TestClientFactory(jsoned message)
    """
    for server in servers:
        #point = TCP4ClientEndpoint(reactor, server[1], server[2])
        #d = connectProtocol(point, TestClientFactory())
    """
    #def callWhenRunning(callable, *args, **kw): (source)
    for server in servers:
        reactor.connectTCP(server[1],server[2],clientfactory)
    reactor.callWhenRunning(execute_from_command_line, sys.argv)
    reactor.run()
    """
    t = Thread(target=reactor.run, args=(False,))		#might not need a thread if callWhenRunning works
    t.daemon = True
    t.start()
    
    print "Does it reach this"
    while True:
        time.sleep(1)
        """

if __name__ == '__main__':
    main()