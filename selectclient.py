'''
Created on Feb 2, 2015

@author: Daniel
'''
import socket
import sys
import time
import json
import math



"""
def multisave(classname, obj):
  for currentserver in servers: 
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	server_address = (currentserver[1],currentserver[2])
	sock.connect(server_address)
	message = 
	#send ACC message to currentserver
	#wait to receive
  #IF GETS ENOUGH LRN MESSAGES THEN SAVE
  for currentserver in servers:
	super(classname,obj).save(using=currentserver[0])
	#close connection?
	seqno += 1
"""

savehistory = []    #history of all the saves in order in case server crashes and needs to update

#sequence number
seqno = 1
classname = 'Election'
servers = [('default','192.168.170.131',8000),('server1','192.168.170.130',8000)]
roundnumber = 1
highestroundnumber = 0
currentleader = 'server1'

#messages are organized <ACC/LRN, message, sequence number, servername/roundnumber?>
#<PREP, servername>
#<PROM, message/None, servername>
message1 = json.dumps(('ACC',classname, seqno, roundnumber)) #jsonify for socket sending
seqno += 1
message2 = json.dumps(('ACC',classname,seqno, roundnumber))
"""
messagetuples = [ ('ACC','server 1',1),
             ('ACC','server 1',2),
             ]
message1 = json.dumps(messagetuples[0])
message2 = json.dumps(messagetuples[1])
print message1
print message2
"""
messagelist = [message1, message2]

#amount_expected = len(''.join(messages))
lrns_expected = math.ceil(len(servers)/2.0)  #self server counts as an implicit LRN (REMEMBER TO ADD -1 WHEN INTRODUCING MORE SERVERS)
proms_expected = math.ceil(len(servers)/2.0)     #self server counts as an implicit PROM
print "lrns expected: %d" % lrns_expected


#try to establish a connection with all servers to send a message
def sendmessage(message):
    sockstimedout = 0   #counter for number of sockets timed out
    lrns_received = 0   #counter for number of LRNs received
    proms_received = 0
    
    #-----------
    #FOR SERVER IN SERVERS DO THIS
    #-----------
    for server in servers:
        try:
            # Create a TCP/IP socket
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

            # Connect the socket to the port where the server is listening
            server_address = (server[1], server[2]) #change to account for different servers in server list
            print >>sys.stderr, 'connecting to %s port %s' % server_address

            #You just need to use the socket settimeout() method before attempting the connect(), 
            #please note that after connecting you must settimeout(None) to set the socket into blocking mode, 
            #such is required for the makefile . Here is the code I am using:
            try:
                sock.settimeout(10)
                sock.connect(server_address)
                
                sock.settimeout(None)
                time.sleep(1)
                
                # Send data
                print >>sys.stderr, 'sending "%s"' % message
                sock.sendall(message) 
                #Unlike send(), this method continues to send data from string until either all data has been sent or an error occurs.
                #None is returned on success. On error, an exception is raised, and there is no way to determine how much data, 
                #if any, was successfully sent.
                 
                if json.loads(message)[0] == 'ACC':     #look for LRN response
                    # Look for the response
                    timeout = time.time() + 10
                    #while lrns_received < lrns_expected:
                    while lrns_received < lrns_expected: 
                        data = sock.recv(len(message))
                        if time.time() > timeout:
                            print "LRN receive timeout"
                            continue
                        lrns_received += 1
                        json_data = json.loads(data)
                        print json_data
                        print >>sys.stderr, 'received "%s"' % json_data[0]
                        time.sleep(1)
                       #print >>sys.stderr, 'received "%s"' % tupledata
                       
                    if lrns_received >= lrns_expected:
                        #save here
                        #savehistory.append(obj)
                        multisave(json.loads(message)[1],messagelist)
                        lrns_received = 0
                        print "SAVE"
                    else:
                        print "Not enough LRNs, resending message"
                        sendmessage(message) #retry transmission in case of message loss
                        
                elif json.loads(message)[0] == 'PREP':      #look for PROM message
                    timeout = time.time() + 10
                    while proms_received < proms_expected: 
                        data = sock.recv(1024)
                        if time.time() > timeout:
                            print "PROM receive timeout"
                            break
                        proms_received += 1
                        json_data = json.loads(data)
                        print json_data
                        print >>sys.stderr, 'received "%s"' % json_data[0]
                        time.sleep(1)
                       #print >>sys.stderr, 'received "%s"' % tupledata
                       

                    json_data = json.loads(data)
                    print json_data
                    if proms_received >= proms_expected:
                        classname = json_data[1]
                        roundnumber = json_data[2]
                        currentleader = findleader(roundnumber)
                        proms_received = 0 
                        if classname is None:       #no message
                            pass
                        else:                               #message needs to be reACCed
                            seqno += 1
                            sendmessage(json.dumps('ACC', classname, seqno, roundnumber))
                    else:
                        print "Not enough PROMs"
                    
                    
                else:
                    print "Invalid message type"
                    continue
                
            except (socket.error, socket.timeout):        #may need to change into try/except 
                sockstimedout += 1
                print "Socket connection error"
                continue
            #else:


        finally:
            print >>sys.stderr, 'closing socket'
            sock.close()
        
def multisave(classname, obj):
    savehistory.append((classname, obj))
    
def findleader(roundnumber):        #figure out leader name from round number
    serverno = len(servers) % roundnumber
    return servers[serverno][0]
    
sendmessage(message1)
sendmessage(message2)

	
