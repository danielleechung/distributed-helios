'''
Created on Feb 1, 2015

@author: Daniel
'''
import select, socket, sys, Queue
import threading, time
import json

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setblocking(0)
server_address = ('192.168.170.131',8000)
print >>sys.stderr, 'starting up on %s port %s' % server_address
server.bind(server_address)
server.listen(5)

# Sockets from which we expect to read
inputs = [ server ]

# Sockets to which we expect to write
outputs = [ ]

# Outgoing message queues (socket:Queue)
message_queues = {}

class serverThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name
    def run(self):
        print "Starting " + self.name
        runserver()
        print "Exiting " + self.name

def runserver():
    highestroundnumber = 0  #for leadership changing
    currentleader = 'server1'
    potentialrequest = [highestroundnumber, None]  #let new leader know of executed requests from previous server
    
    while inputs:
        # Wait for at least one of the sockets to be ready for processing
        print >>sys.stderr, '\nwaiting for the next event'
        timeout = 5
        readable, writable, exceptional = select.select(inputs, outputs, inputs, timeout)

        if not (readable or writable or exceptional):
            print >>sys.stderr, '  timed out, do some other work here'
            continue

        # Handle inputs
        for s in readable:

            if s is server:
                # A "readable" server socket is ready to accept a connection
                connection, client_address = s.accept()
                print >>sys.stderr, 'new connection from', client_address
                connection.setblocking(0)
                inputs.append(connection)

                # Give the connection a queue for data we want to send
                message_queues[connection] = Queue.Queue()

            else:
                data = s.recv(1024)
                if data:
                    # A readable client socket has data
                    print >>sys.stderr, 'received "%s" from %s' % (data, s.getpeername())
                    
                    #data = [ ACC/LRN, classname, seqno ]
                    json_data = json.loads(data)
                    messagetype = json_data [0]     #ACC/LRN/PREP/PROM
                    if messagetype == 'ACC':
                        #put LRN message in message queue back
                        highestroundnumber = json_data[3]
                        classname = json_data[1]
                        potentialrequest = [highestroundnumber, classname]
                        response = json_data
                        response[0] = 'LRN'
                        print response
                        message_queues[s].put(json.dumps(response))
                    elif messagetype == 'PREP':  #leadership change request
                        #ignore all messages sent by the old leadership
                        #send back <PROM, message, newleader> if there's a previous message
                        #send back <PROM, None, newleader> if there isn't
                        if json_data[1] > highestroundnumber:    #if the new server wants leadership, needs to have a higher round number than the old one
                            highestroundnumber = json_data[1]
                            if potentialrequest[1] == None:                    
                                response = json.dumps(('PROM',None, highestroundnumber))
                            else:
                                classname = json_data[1]
                                response = json.dumps(('PROM',classname, highestroundnumber))
                            message_queues[s].put(json.dumps(response))
                        else:   #new server doesn't have higher round number, don't acknowledge
                            pass
                    # Add output channel for response
                    else:
                        #echo
                        message_queues[s].put(data)
                    if s not in outputs:
                        outputs.append(s)

                else:
                    # Interpret empty result as closed connection
                    print >>sys.stderr, 'closing', client_address, 'after reading no data'
                    # Stop listening for input on the connection
                    if s in outputs:
                        outputs.remove(s)
                    inputs.remove(s)
                    #s.close()

                    # Remove message queue
                    del message_queues[s]

        # Handle outputs
        for s in writable:
            try:
                next_msg = message_queues[s].get_nowait()
            except Queue.Empty:
                # No messages waiting so stop checking for writability.
                print >>sys.stderr, 'output queue for', s.getpeername(), 'is empty'
                outputs.remove(s)
            else:
                print >>sys.stderr, 'sending "%s" to %s' % (next_msg, s.getpeername())
                s.send(next_msg)

        # Handle "exceptional conditions"
        for s in exceptional:
            print >>sys.stderr, 'handling exceptional condition for', s.getpeername()
            # Stop listening for input on the connection
            inputs.remove(s)
            if s in outputs:
                outputs.remove(s)
            s.close()

            # Remove message queue
            del message_queues[s]

            
server1 = serverThread("Server 1")
server1.start()

i = 0
while 1:
    print i
    i = i + 1
    time.sleep(1)