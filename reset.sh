#!/bin/bash
dropdb helios
createdb helios
python manage.py syncdb
python manage.py migrate
echo "from helios_auth.models import User; User.objects.all().delete(); print User.objects.all().count()" | python manage.py shell
echo "from helios_auth.models import User; User.objects.create(user_type='google',user_id='danielleechung@gmail.com', info={'name':'Daniel Chung'})" | python manage.py shell
